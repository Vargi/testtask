# README #

Supermetrics testtask.

PHP 7.4 + MySQL database 


### Set up ###

* Install composer. (only the autoload and PHPUnit is used.)

* Database can be configured in the \config\database.php

* Run Migration to create the necessary table: \db\Migrate.php

### Run ###

Entry point: \bin\runPosts.php

### Comments ###

* The d. point in the stats (d. - Average number of posts per user per month) wasn't clear for me I created 2 version of it.

* I created some automated PHPUnit test, but only a few.