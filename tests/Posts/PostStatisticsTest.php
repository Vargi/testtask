<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 12:37
 */

use PHPUnit\Framework\TestCase;
use Src\Posts\PostsStatistics;

class PostStatisticsTest extends TestCase
{

    public function testAverageCharPerMonth() : void
    {
        $postStatistics = $this->createMock(PostsStatistics::class);
        $this->assertIsString($postStatistics->averageCharPerMonth());

    }
}