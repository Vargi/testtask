<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 11:40
 */


use PHPUnit\Framework\TestCase;
use Src\Posts\PostEntity;
use Src\Entity\EntityInterface;

class PostsEntityTest extends TestCase
{

    public function testCreateFromImportReturn() : void
    {

        $post = (object)[   'id' => 'post5f967ff4929b6_a65e0c7e',
                            'from_name' => 'Carson Smithson',
                            'from_id' => 'user_5',
                            'message' => 'terrify director egg white trend bat dynamic underline expertise plane innocent money house abuse put herb bury harmful rob hypnothize waist interest egg white porter fabricate mastermind promotion elite future location coincide braid coin mild facility contrast indulge morsel sweet animal climb energy shy permission platform bother knock chest railroad shy fist bury bother tile theft heaven Europe sweet line cave absent sow abuse sulphur hilarious confusion crosswalk donor tap dimension chief virgin charter underline hole state lend memorandum charm omission information pump alcohol symptom rush beg symbol establish sample flower full',
                            'type' => 'status',
                            'created_time' => '2020-10-26T03:33:47+00:00'];

        $this->assertInstanceOf(EntityInterface::class, PostEntity::createFromImport($post));
        $this->assertInstanceOf(PostEntity::class, PostEntity::createFromImport($post));
    }

    public function testGetTable() : void
    {
        $this->assertIsString(PostEntity::getTable());
        $this->assertEquals('posts', PostEntity::getTable());
    }

    public function testGetInsertFields() : void
    {
        $this->assertIsArray(PostEntity::getInsertFields());
    }

    public function testGetInsertData() : void
    {
        $post = (object)[   'id' => 'post5f967ff4929b6_a65e0c7e',
            'from_name' => 'Carson Smithson',
            'from_id' => 'user_5',
            'message' => 'terrify director egg white trend bat dynamic underline expertise plane innocent money house abuse put herb bury harmful rob hypnothize waist interest egg white porter fabricate mastermind promotion elite future location coincide braid coin mild facility contrast indulge morsel sweet animal climb energy shy permission platform bother knock chest railroad shy fist bury bother tile theft heaven Europe sweet line cave absent sow abuse sulphur hilarious confusion crosswalk donor tap dimension chief virgin charter underline hole state lend memorandum charm omission information pump alcohol symptom rush beg symbol establish sample flower full',
            'type' => 'status',
            'created_time' => '2020-10-26T03:33:47+00:00'];

        $postEntity = PostEntity::createFromImport($post);

        $ret = $postEntity->getInsertData();
        $this->assertIsArray($ret);
        $this->assertArrayHasKey(2,$ret);
        $this->assertIsInt($ret[2]);
    }
}