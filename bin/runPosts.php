<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 10:26
 */

require_once(__DIR__ . '/../vendor/autoload.php');

use Core\Database;
use Src\Posts\PostsImporter;
use Src\Posts\PostEntity;
use Src\Logger\ErrorFileLoggerFactory;
use Src\Posts\PostsStatistics;
use Src\OutputFormatter\JsonFormatter;

$database = new Database();
$db = $database->connect();
$p = new PostEntity();
try {
    $postsImporter = new PostsImporter($db);
    $postsImporter->runImport();
}
catch (Exception $e) {
    $loggerFactory = new ErrorFileLoggerFactory();
    $logger = $loggerFactory->createLogger();
    $logger->log($e);
    echo 'Unsuccessful';
    exit;
}

$postStatistics = new PostsStatistics($db, new JsonFormatter());
echo $postStatistics->averageCharPerMonth() . "\n";
echo $postStatistics->longestCharPerMonth() . "\n";
echo $postStatistics->totalPostsPerWeek() . "\n";
//I wasn't sure about the last statistics so I created 2 different solution
echo $postStatistics->averagePostsPerUserMonth() . "\n";
echo $postStatistics->averageMonthlyPostsPerUser() . "\n";