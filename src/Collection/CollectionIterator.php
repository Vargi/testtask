<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 10:36
 */

namespace Src\Collection;

use Src\Entity\EntityInterface;

//It is not used in the code, but I do not want to remove because it could be helpfull in the future
class CollectionIterator implements \Iterator
{

    private EntityCollection $entityCollection;
    private int $position = 0;

    /**
     * @param EntityCollection $entityCollection
     */
    public function __construct(EntityCollection $entityCollection) {
        $this->position = 0;
        $this->entityCollection = $entityCollection;
    }


    public function rewind() : void {
        $this->position = 0;
    }

    /**
     * @return EntityInterface
     */
    public function current() : EntityInterface {
        return $this->entityCollection->get($this->position);
    }

    /**
     * @return int
     */
    public function key() : int {
        return $this->position;
    }

    public function next() : void {
        ++$this->position;
    }

    /**
     * @return bool
     */
    public function valid() : bool {
        return !is_null($this->entityCollection->get($this->position));
    }
}