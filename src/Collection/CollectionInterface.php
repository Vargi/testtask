<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 10:41
 */

namespace Src\Collection;

use Src\Entity\EntityInterface;

interface CollectionInterface
{
    /**
     * @param EntityInterface $entity
     *
     * @return void
     */
    public function add(EntityInterface $entity): void;

    /**
     * @param int $id
     *
     * @return null/EntityInterface
     */
    public function get(int $id): ?EntityInterface;

    /**
     *
     * @return CollectionIterator
     */
    public function getIterator(): CollectionIterator;

    /**
     * Save the collection to the database. return the number of the inserted rows
     *
     * @return int
     */
    public function insertCollection() : int;
}