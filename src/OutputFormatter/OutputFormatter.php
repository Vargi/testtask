<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 10:22
 */

namespace Src\OutputFormatter;


interface OutputFormatter
{

    /**
     * @param array $data
     *
     * @return string
     */
    public function format(array $data) : string;
}