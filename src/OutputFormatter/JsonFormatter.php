<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 10:23
 */

namespace Src\OutputFormatter;


class JsonFormatter implements OutputFormatter
{

    /**
     * @param array $data
     *
     * @return string
     */
    public function format(array $data) : string
    {

        return json_encode($data,JSON_NUMERIC_CHECK );
    }
}