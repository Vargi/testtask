<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 20:19
 */

namespace Src\Posts;

use Src\Entity\EntityCollection;
use Core\HttpRequest;
use Src\Importer\ImporterInterface;

class PostsImporter implements ImporterInterface
{
    private const PAGECOUNT = 100;

    private \mysqli $db;

    private EntityCollection $postsCollection;


    /**
     * @param \mysqli $db
     */
    public function __construct(\mysqli $db)
    {
        $this->db = $db;
        $this->postsCollection = new EntityCollection($db, 'Src\Posts\PostEntity');
    }

    /**
     * Import the new posts to the database
     *
     * @return void
     */
    public function runImport() : void
    {
        $httpRequest = new HttpRequest();

        $token = $this->getToken($httpRequest);

        $page = 1;
        do {
            $posts = $this->getNewPosts($httpRequest, $token, $page);
            if (!empty($posts)) {
                foreach ($posts as $post) {
                    $this->postsCollection->add(PostEntity::createFromImport($post));
                }
                $insertedRows = $this->postsCollection->insertCollection();
                $page += 1;
            }
        } while ($insertedRows == self::PAGECOUNT && !empty($posts));


    }


    /**
     * @param object $postData
     *
     * @return EntityInterface
     */
    private function getToken(HttpRequest $httpRequest) : string
    {
        $url = 'https://api.supermetrics.com/assignment/register';
        $parameters = [ 'client_id' => 'ju16a6m81mhid5ue1z3v2g0uh',
                        'email'     => 'vargaviktoria.vv@gmail.com',
                        'name'      => 'Viktoria Varga'];
        $resp = $httpRequest->post($url,$parameters);
        $resp = json_decode($resp);
        if ($resp === false) {
            throw new \Exception('getToken - Response is not json!');
        }
        if (isset($resp->error)) {
            throw new \Exception('getToken - ' . json_encode($resp->error));
        }
        if (isset($resp->data->sl_token)) {
            return $resp->data->sl_token;
        }
        else {
            throw new \Exception('getToken - Missing sl_token' );
        }

    }

    /**
     * @param HttpRequest $httpRequest
     * @param string $token
     * @param int $page
     *
     * @return array
     */
    private function getNewPosts(HttpRequest $httpRequest, string $token, int $page): array
    {
        $url = 'https://api.supermetrics.com/assignment/posts';
        $parameters = [ 'sl_token'  => $token,
                        'page'      => $page];
        $resp = $httpRequest->get($url,$parameters);
        $resp = json_decode($resp);
        if ($resp === false) {
            throw new \Exception('getNewPosts - Response is not json!');
        }

        if (isset($resp->error)) {
            throw new \Exception('getToken - ' . json_encode($resp->error));
        }
        if (isset($resp->data->page) && $resp->data->page == $page) {
            return $resp->data->posts;
        }
        else {
            return [];
        }


    }

}