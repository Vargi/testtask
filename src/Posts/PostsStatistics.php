<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 23:10
 */

namespace Src\Posts;

use Src\OutputFormatter\OutputFormatter;

class PostsStatistics
{
    private \mysqli $db;
    private OutputFormatter $formatter;

    /**
     * @param \mysqli $db
     * @param OutputFormatter $formatter
     */
    public function __construct(\mysqli $db, OutputFormatter $formatter)
    {
        $this->db = $db;
        $this->formatter = $formatter;
    }

    //a. - Average character length of posts per month
    /**
     * @return string
     */
    public function averageCharPerMonth() : string
    {
        $result = $this->db->query("SELECT YEAR(created_time) as year, MONTH(created_time) as month, (SUM(message_length)/count(id)) as avg_message_length FROM posts GROUP BY YEAR(created_time), MONTH(created_time)");
        return $this->formatter->format(['averageCharPerMonth' => $result->fetch_all(MYSQLI_ASSOC)]);
    }

    //b. - Longest post by character length per month
    /**
     * @return string
     */
    public function longestCharPerMonth() : string
    {
        $result = $this->db->query("SELECT YEAR(created_time) as year, MONTH(created_time) as month, MAX(message_length) longest_post_length FROM posts GROUP BY YEAR(created_time), MONTH(created_time)");
        return $this->formatter->format(['longestCharPerMonth' => $result->fetch_all(MYSQLI_ASSOC)]);
    }

    //c. - Total posts split by week number
    /**
     * @return string
     */
    public function totalPostsPerWeek() : string
    {
        $result = $this->db->query("SELECT YEAR(created_time) year, WEEK(created_time) week, count(id) posts_number FROM posts GROUP BY YEAR(created_time), WEEK(created_time)");
        return $this->formatter->format(['totalPostsPerWeek' => $result->fetch_all(MYSQLI_ASSOC)]);
    }

    //d. - Average number of posts per user per month
    // d.1 - for every month posts number divided by users number who created posts
    /**
     * @return string
     */
    public function averagePostsPerUserMonth() : string
    {
        $result = $this->db->query("SELECT YEAR(created_time) year, MONTH(created_time) month, count(id)/count(DISTINCT from_id) avg_user_month FROM posts GROUP BY YEAR(created_time), MONTH(created_time)");
        return $this->formatter->format(['averagePostsPerUserMonth' => $result->fetch_all(MYSQLI_ASSOC)]);
    }

    //d.2 - Average monthly posts per user
    /**
     * @return string
     */
    public function averageMonthlyPostsPerUser() : string
    {
        $result = $this->db->query("SELECT from_id, count(id)/(select count(DISTINCT MONTH(p.created_time)) from posts p) avg_month FROM posts GROUP BY from_id");
        return $this->formatter->format(['averageMonthlyPostsPerUser' => $result->fetch_all(MYSQLI_ASSOC)]);
    }

//
}