<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 10:47
 */

namespace Src\Posts;

use Src\Entity\EntityInterface;

class PostEntity implements EntityInterface
{
    private const DB_FIELDS = array(
        'post_id' => 'postId',
        'from_id' => 'fromId',
        'message_length' => 'messageLength',
        'type' => 'type',
        'created_time' => 'createdTime'
    );

    private const DB_TABLE = 'posts';

    private string $postId;
    private string $fromId;
    private int $messageLength;
    private string $type;
    private string $createdTime;

    /**
     * @param object $postData
     *
     * @return EntityInterface
     */
    public static function createFromImport(object $postData) : EntityInterface
    {
        $postEntity = new self();
        $postEntity->postId = $postData->id;
        $postEntity->fromId = $postData->from_id;
        $postEntity->messageLength = iconv_strlen($postData->message);
        $postEntity->type = $postData->type;
        $datetime = new \DateTime($postData->created_time);
        $postEntity->createdTime = $datetime->format('Y-m-d H:i:s');

        return $postEntity;
    }

    /**
     * @return array
     */
    public static function getInsertFields() : array
    {
        return array_keys(self::DB_FIELDS);
    }

    /**
     * @return string
     */
    public static function getTable() : string
    {
        return self::DB_TABLE;
    }

    /**
     * @return array
     */
    public function getInsertData() : array
    {
        $data = [];
        foreach (self::DB_FIELDS as $value) {
            $data[] = $this->$value;
        }
        return $data;
    }

}