<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 9:55
 */

namespace Src\Importer;

interface ImporterInterface
{
    public function runImport() : void;

}