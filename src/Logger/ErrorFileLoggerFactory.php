<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 10:09
 */

namespace Src\Logger;


class ErrorFileLoggerFactory
{
    private const FILEPATH = __DIR__ . '/../../log/error.log';

    /**
     * @return Logger
     */
    public function createLogger(): LoggerInterface
    {
        return new FileLogger(self::FILEPATH);
    }
}