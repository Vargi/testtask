<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 10:04
 */

namespace Src\Logger;


interface LoggerInterface
{
    /**
     * @param string $message
     */
    public function Log(string $message);
}