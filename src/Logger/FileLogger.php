<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 10:05
 */

namespace Src\Logger;


class FileLogger implements LoggerInterface
{
    private string $filePath;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @param string $message
     */
    public function Log(string $message)
    {
        file_put_contents($this->filePath,date('Y-m-d H:i:s') . ' - ' . $message . "\n" , FILE_APPEND);
    }
}