<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 27.
 * Time: 10:08
 */

namespace Src\Logger;


interface LoggerFactory
{
    /**
     * @return Logger
     */
    public function createLogger(): LoggerInterface;
}