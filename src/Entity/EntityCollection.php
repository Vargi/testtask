<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 10:42
 */

namespace Src\Entity;


use Src\Collection\CollectionInterface;
use Src\Collection\CollectionIterator;

class EntityCollection implements CollectionInterface
{

    private \mysqli $db;
    private string $entityClass;
    private array $entities;
    private array $entitiesInsertData;

    /**
     * @param \mysqli $db
     * @param string $entityClass
     */
    public function __construct(\mysqli $db, string $entityClass)
    {
        $this->db = $db;
        $this->entityClass = $entityClass;
    }

    /**
     * @param EntityInterface $entity
     *
     * @return void
     */
    public function add(EntityInterface $entity): void
    {
        $this->entities[] = $entity;
        $this->entitiesInsertData[] = "'".implode("','",$entity->getInsertData())."'";
    }

    /**
     * @param int $id
     *
     * @return null/EntityInterface
     */
    public function get(int $index): ?EntityInterface
    {
        if (isset($this->entities[$index])) {
            return $this->entities[$index];
        }
        else {
            return null;
        }
    }

    /**
     *
     * @return CollectionIterator
     */
    public function getIterator(): CollectionIterator {
        return new CollectionIterator($this);
    }

    /**
     * Save the collection to the database. return the number of the inserted rows
     *
     * @return int
     */
    public function insertCollection() : int
    {
        if (!empty($this->entitiesInsertData)) {
            $queryString = "INSERT IGNORE INTO " . $this->entityClass::getTable() . " ";
            $queryString .= "(" . implode(',', $this->entityClass::getInsertFields()) . ") ";
            $queryString .= "VALUES (";
            $queryString .= implode('),(', $this->entitiesInsertData);
            $queryString .= ")";
            $this->db->query($queryString);

            $this->entities = [];
            $this->entitiesInsertData = [];
            return $this->db->affected_rows;
        }
        return 0;
    }

}