<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 10:32
 */

namespace Src\Entity;


interface EntityInterface
{
    /**
     * @param object $postData
     *
     * @return EntityInterface
     */
    public static function createFromImport(object $postData) : EntityInterface;

    /**
     * @return array
     */
    public static function getInsertFields() : array;

    /**
     * @return string
     */
    public static function getTable() : string;

    /**
     * @return array
     */
    public function getInsertData() : array;
}