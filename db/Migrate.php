<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 9:29
 */

require __DIR__ . '/../vendor/autoload.php';

use Core\Database;

$database = new Database();
$db = $database->connect();

$db->query("CREATE TABLE IF NOT EXISTS `posts` ( 
                      `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT , 
                      `post_id` VARCHAR(255) NOT NULL , 
                      `from_id` VARCHAR(255) NOT NULL , 
                      `message_length` INT UNSIGNED NOT NULL , 
                      `type` VARCHAR(255) NOT NULL , 
                      `created_time` DATETIME NOT NULL , 
                      PRIMARY KEY (`id`), 
                      INDEX `created_time` (`created_time`), 
                      INDEX `message_length` (`message_length`), 
                      INDEX `from_id` (`from_id`), 
                      UNIQUE `post_id` (`post_id`)
                      ) ENGINE = InnoDB;
");
