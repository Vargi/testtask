<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 9:31
 */

namespace Core;

class Database
{
    private array $params;

    private \mysqli $dbConn;

    public function __construct()
    {
        // this is the configuration file
        require_once(__DIR__ . '/../config/database.php');
        $this->params = $db_params;
    }

    /**
     * Create mysqli db connection
     *
     * @return \mysqli
     */
    public function connect() : \mysqli
    {

        $this->dbConn = new \mysqli($this->params['host'], $this->params['username'], $this->params['password'], $this->params['database']);
        if ($this->dbConn->connect_errno) {
            die('DB connection failed');
        }
        return $this->dbConn;
    }
}