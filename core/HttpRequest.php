<?php
/**
 * Created by PhpStorm.
 * User: Viki
 * Date: 2020. 10. 26.
 * Time: 11:46
 */

namespace Core;


class HttpRequest
{

    /**
     * @param string $url
     * @param array $parameters
     *
     * @return mixed
     */
    public function post(string $url, array $parameters = [])
    {
        $ch = curl_init($url);

        curl_setopt($ch,CURLOPT_POST, true);
        if (!empty($parameters)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        if ($response === false) {
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }

    /**
     * @param string $url
     * @param array $parameters
     *
     * @return mixed
     */
    public function get(string $url, array $parameters = [])
    {
        if (!empty($parameters)) {
            $url .= "?" . http_build_query($parameters);
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        if ($response === false) {
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }
}